import Nav from './Nav';
import React from 'react';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm'
import AttendConferenceForm from './AttendConferenceForm'
import PresentationForm from './PresentationForm'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from './MainPage'


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <div className="container">
      <React.Fragment>
        <Nav />
      </React.Fragment>
    </div>
    <Routes>
      <Route index element={<MainPage />} />
    </Routes>
    <Routes>
      <Route path="/conferences/new" element={<ConferenceForm />} />
    </Routes>
    <Routes>
      <Route path="/attendees/new" element={<AttendConferenceForm />} />
    </Routes>
    <Routes>
      <Route path="/locations/new" element={<LocationForm />} />
    </Routes>
    <Routes>
      <Route path="/attendees" element={<AttendeesList attendees={props.attendees}/>} />
    </Routes>
    <Routes>
      <Route path="/presentations/new" element={<PresentationForm />} />
    </Routes>
      
    </BrowserRouter>
  );
}

export default App;